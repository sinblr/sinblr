#  Sinblr Mastodon  #

 Welcome to the sinblr Mastodon Github repository, the new Tumblr alternative for sexy minded people!


As stated above, sinblr. intends to be a new home to all previous Tumblr users that were pushed of the site, by their new NSFW policies. We offer a place to post your photos, videos and stories, thus we have increased the default character limit of posts to 10.000 characters.

We try to be an open community that can be home to every kinkster regardless of fetish or tendency. That is why, we do not condone racial or political slurs and hate speech and mandate everyone to tag their content appropriately!

## Update from Mastodon source ##

Updating from Mastodon (or from an earlier `sinblr` version) is exactly like updating from one Mastodon version to another, and will in general require the following steps:

1\. Switch to sinblr, for instance by: a. adding a new remote `git remote add sinblr https://github.com/sinblr-com/sinblr`b. fetching it (`git fetch sinblr`) c. switching to the `master`branch from that repo (`git checkout sinblr/master`)\
2\. Fetch the source code (typically, `git pull`)\
3\. Install dependencies: `bundle install && yarn install`\
4\. Run the pre-deployment database migrations: `RAILS_ENV=production SKIP_POST_DEPLOYMENT_MIGRATIONS=true bundle exec rails db:migrate`\
5\. Pre-compile static assets: `RAILS_ENV=production bundle exec rails assets:precompile`

Due to sinblr shipping with two front-end flavours, this step requires more resources than it does on mainline Mastodon.

6\. Restart the services: `systemctl reload mastodon-web && systemctl restart mastodon-{sidekiq,streaming}`\
7\. Clean Rails' cache: `RAILS_ENV=production bin/tootctl cache clear`\
8\. Run the post-deployment database migrations: `RAILS_ENV=production bundle exec rails db:migrate`

So here's the deal: we all work on this code, and then it runs on sinblr.com and anyone who uses that does so absolutely at their own risk.

- You can view documentation for this project at [glitch-soc.github.io/docs/](https://glitch-soc.github.io/docs/).
- And contributing guidelines are available [here](CONTRIBUTING.md) and [here](https://glitch-soc.github.io/docs/contributing/).
